<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <link href="style.css" type="text/css" rel="stylesheet">

    <title>Form Mahasiswa</title>
</head>

<body>
    <div class="form-holder">
        <div class="form-content">
            <div class="form-items">
                <h3>Form Data Mahasiswa</h3>
                <p>Masukan data Mahasiswa</p>
                <form action="hasil_input.php" method="POST">
                    <div class="col-md-12">
                        <input class="form-control" type="text" name="nama" placeholder="Nama Lengkap" required>
                    </div>
                    <div class="col-md-12">
                        <input class="form-control" type="text" name="matkul" placeholder="Mata Pelajaran" required>
                    </div>
                    <div class="col-md-12">
                        <input class="form-control" type="number" name="tugas" min="0" max="100"
                            placeholder="Nilai Tugas" required>
                    </div>
                    <div class="col-md-12">
                        <input class="form-control" type="number" name="uts" min="0" max="100" placeholder="Nilai UTS"
                            required>

                    </div>
                    <div class="col-md-12">
                        <input class="form-control" type="number" name="uas" min="0" max="100" placeholder="Nilai UAS"
                            required>
                    </div>
                    <div class="row">
                        <div class="col-3">
                            <div class="form-button mt-3">
                                <button id="submit" type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-button mt-3">
                                <button id="reset" type="reset" class="btn btn-primary">Reset</button>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
    </script>


</body>

</html>