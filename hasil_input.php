<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Hasil Data Mahasiswa</title>
</head>

<body>

    <div class="form-holder">
        <div class="form-content">
            <div class="form-items">
                <table class="table  text-white">
                    <?php
                    $hari_ini = date("d-m-y");
                    $jam = date("H:i:s");
                    $nama = $_POST['nama'];
                    $mk = $_POST["matkul"];
                    $tugas = $_POST['tugas'];
                    $uts = $_POST['uts'];
                    $uas = $_POST['uas'];

                    $na = (0.15 * $tugas) + (0.35 * $uts) + (0.50 * $uas);

                    if ($na <= 50 && $na >= 0) {
                        $nh = 'D';
                    } else if ($na > 50 && $na <= 70) {
                        $nh = 'C';
                    } else if ($na > 70 && $na < 90) {
                        $nh = 'B';
                    } else if ($na >= 90 && $na <= 100) {
                        $nh = 'A';
                    }
                    ?>
                    <h3 class="text-center">Nilai Mahasiswa</h3>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Tanggal</th>
                            <th>Waktu</th>
                            <th>Nama Mahasiswa</th>
                            <th>Mata Pelajaran</th>
                            <th>Nilai UTS</th>
                            <th>Nilai UAS</th>
                            <th>Nilai Tugas</th>
                            <th>Total Nilai</th>
                            <th>Grade</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <tr class="text-center">
                            <td><?= $i; ?></td>
                            <td><?php echo ($hari_ini); ?></td>
                            <td><?php echo ($jam); ?></td>
                            <td> <?php echo ($nama);    ?></td>
                            <td> <?php echo ($mk);  ?></td>
                            <td> <?php echo ($uts);    ?></td>
                            <td> <?php echo ($uas);    ?></td>
                            <td> <?php echo ($tugas);    ?></td>
                            <td><?php echo ($na);      ?></td>
                            <td> <?php echo ($nh);    ?></td>
                            <?php $i++; ?>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>



    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
    </script>


</body>

</html>